package eu.uniroma2.alumni.nicola_codipietro;

import java.util.Scanner;
import eu.uniroma2.alumni.nicola_codipietro.Doubler;

/**
 * Double our input.
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner scan = new Scanner(System.in);
        Doubler dbl = new Doubler(scan.nextInt());
	dbl.makeItDouble();
        System.out.println("Double is: " + Integer.toString(dbl.getNumber()));
    }
}
