package eu.uniroma2.alumni.nicola_codipietro;

public class Doubler {
    private int number;

    public Doubler() {}

    public Doubler(int num) {
        this.number = num;
    }

    public int getNumber() {
        return this.number;
    }

    public void setNumber(int num) {
        this.number = num;
    }

    public void makeItDouble() {
        this.number = this.number * 2;
    }
}
